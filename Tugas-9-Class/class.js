
console.log("----------Release 0--------")

class Animal {
  constructor (name, legs, cold_blooded){
    this._name = name;
    this._legs = legs;
    this._cold_blooded = cold_blooded;
  }
  get name (){
    return this._name
  }
  get legs(){
   return this._legs = 4
   }
   get cold_blooded(){
     return this._cold_blooded = false
   }
}
var sheep = new Animal("shaun")

console.log(sheep.name) //"shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) //false

console.log("-----------Release 1------")

class Ape extends Animal{
  constructor (name, legs,yell){
    super(name);
    this._legs = legs
    this._yell = yell
    }
    get legs(){
      return this._legs = 2
    }
    get yell(){ 
      return this._yell = function yell(){ //function yell
      console.log("Auooo")
    }
    }
  }

var sungokong = new Ape("kera sakti")
sungokong.yell() //Auooo
console.log(sungokong.name) //kera sakti
console.log(sungokong.legs) // 2
console.log(sungokong.cold_blooded) // false

class Frog extends Animal{
  constructor (name, jump, cold_blooded){
    super(name);
    this._jump = jump //function jump
    this._cold_blooded = cold_blooded //frog is cold blooded
  }
  get jump(){
    return this._jump = function jump(){ //function jump
      console.log("hop hop")
    }
  }
  get cold_blooded(){
    return this._cold_blooded = true
  }
}
var kodok = new Frog("buduk")
kodok.jump()
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)

console.log("-----Function to Class-----")

class Clock{
    constructor({template}){
        this.template = template;

    }

        render(){
                  var date = new Date();

                  var hours = date.getHours();
                  if (hours < 10) hours = '0' + hours;

                  var mins = date.getMinutes();
                  if (mins < 10) mins = '0' + mins;

                  var secs = date.getSeconds();
                  if (secs < 10) secs = '0' + secs;

                  var output = this.template
                   .replace('h', hours)
                   .replace('m', mins)
                   .replace('s', secs);

                  console.log(output);
            }
        
        
        stop(){
                clearInterval(this.timer);
              };
        
         start(){
                this.render();
                this.timer = setInterval(this.render(), 1000);
              };
        
    }

var clock = new Clock({template: 'h:m:s'});
clock.start();