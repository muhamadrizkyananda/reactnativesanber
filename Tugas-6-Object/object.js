
function arrayToObject(arr) {
    for (var i=0;i<arr.length; i++){
      var birthYear = arr[i][3];
      var now = new Date()
      var thisYear = now.getFullYear()
      var currentAge;
      if (birthYear && thisYear - birthYear > 0){
          currentAge = thisYear - birthYear
      }else{
          currentAge = "Invalid Birth Year"
      }
      var peopleObj1 ={
          firstName : arr[i][0],
          lastName: arr[i][1],
          gender: arr[i][2],
          age: currentAge
      }
  console.log(peopleObj1.lastName + "  " +peopleObj1.firstName + ": {")
  console.log("firstName: " + " ' " + peopleObj1.firstName + " ' " + " , ")
  console.log("lastName:  " + " ' " + peopleObj1.lastName + " ' " + " , ")
  console.log("gender: " + " ' " + peopleObj1.gender + " ' " + " , ")
  console.log("age: "+" ' " + peopleObj1.age + " ' " + "\n" + "} ")
  }
  }
  console.log("========= Soal 1 ==========")
  var people = [["Amagase", "Touma", "male", 2004], ["Ganaha", "Hibiki", "female", 2022], ["Kimura", "Ryu", "male"]]
  arrayToObject(people)
  
  console.log("========= Soal 2 ==========")
  
  function shoppingTime(memberId, money){
      if (!memberId){
          return "Mohon maaf, toko X hanya berlaku untuk member saja"
      }
      else if (money < 50000){
          return "Mohon maaf, uang tidak cukup"
      }else {
          var object = {};
          var purchaseList = [];
          var moneyChange = money
          var produk = {
              sepatuStacattu : "Sepatu Stacattu", //1500000
              bajuZoro : "Baju Zoro", //500000
              bajuHN : "Baju H&N", //250000
              sweaterUniklooh: "Sweater Uniklooh", //175000
              casingHP: "Casing Handphone" //50000
          }
          var check = 0;
  
          for (var i = 0; moneyChange >=50000 && check == 0 ; i++){
              if (moneyChange >= 1500000){
                  purchaseList.push(produk.sepatuStacattu)
                  moneyChange -= 1500000
              }else if (moneyChange >= 500000){
                  purchaseList.push(produk.bajuZoro)
                  moneyChange -= 500000
              }else if (moneyChange >= 250000){
                  purchaseList.push(produk.bajuHN)
                  moneyChange -= 250000
              }else if (moneyChange >= 175000) {
                  purchaseList.push(produk.sweaterUniklooh)
                  moneyChange -= 175000
              }
              else if (moneyChange >= 50000){
                  for ( var j = 0; j <= purchaseList.length-1; j++){
                      if(purchaseList[j] == produk.casingHP){
                          check +=1
                      }
                  }if(check == 0){
                      purchaseList.push(produk.casingHP)
                      moneyChange -= 50000
                  }
              }
          }
      
          object.memberId = memberId
          object.money = money
          object.listPurchased = purchaseList
          object.changeMoney = moneyChange
          return object
      }
  }
  //TEST CASES   
  console.log("---Member ID kosong----")
  console.log(shoppingTime('',60000))
  console.log("---kurang dari 50k-----")
  console  .log(shoppingTime("234JdhweRxa53", 15000))
  console.log("---Uang 2475000------")
  console.log(shoppingTime("1820RzKrnWn08", 2475000))
  console.log("---Uang 170000--------")
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime());
  
  console.log("-------Soal 5 --------")
  
  function naikAngkot (arrPenumpang){
      var rute = ["A", "B", "C", "D", "E", "F"]
      var arrOutput = []
      if(arrPenumpang.length <= 0){
          return []
      }
  
      for(var i=0 ; i < arrPenumpang.length; i++){
          var ruteOutput = {}
          var asal = arrPenumpang [i][1]
          var tujuan = arrPenumpang [i][2]
          var indexAsal;
          var indexTujuan
  
          for (var j = 0; j<rute.length; j++){
              if (rute[j] == asal){
                  indexAsal
              }else if (rute [j] == tujuan){
                  indexTujuan = j
              }
          }
          var bayar = (indexTujuan - indexAsal) * 2000
  
          ruteOutput.penumpang = arrPenumpang
          ruteOutput.naikDari = asal
          ruteOutput.tujuan = tujuan
          ruteOutput.bayar = bayar
  
          arrOutput.push(ruteOutput)
      } return arrOutput
  }
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));