console.log("Soal 1");

function range(startNum, finishNum){
    var array1 = []
    if (startNum < finishNum){
        for(var i = startNum;i <= finishNum; i++){
            array1.push(i)
         }
    }       
    else if (startNum > finishNum){
        for(var i = startNum;i >= finishNum; i--){
            array1.push(i)
        }
    }
    else if (startNum == null , finishNum == null){
              array1 = [-1];
    }
    return array1;

}
console.log(range(1, 10) + "\n")
console.log(range(1) + "\n")
console.log(range(11, 18) + "\n")
console.log(range(54, 50) + "\n")
console.log(range() + "\n")

console.log("Soal 2");

function rangeWithStep(startNum2, finishNum2, step){
    var array2 = [];
    if (startNum2 < finishNum2){
        for(var c = startNum2;c <= finishNum2; c+=step){
            array2.push(c)
         }
    } else if (startNum2 > finishNum2)  {
      for (var c = startNum2; c >= finishNum2; c-=step){
        array2.push(c)
      }
    }
    return array2;     
      
    }
   
console.log(rangeWithStep(1, 10, 2) + "\n ---")
console.log(rangeWithStep(11, 23, 3) + "\n ---")
console.log(rangeWithStep(5, 2, 1) + "\n ---")
console.log(rangeWithStep(29, 2, 4) + "\n ---")

console.log("Soal 3");

function sum(startNum3, finishNum3, step3){
    var array3 = []
    var p;

    if (!step3){
             p = 1;   //tidak ada step menjadi 1
    }else {
             p = step3 //menjadi step
    }

    if (startNum3 > finishNum3){
        var currentNum = startNum3;
        for (var k = 0; currentNum >= finishNum3; k++){
            array3.push(currentNum)
            currentNum -= p
        }
    }else if (startNum3 > finishNum3){
        var currentNum = startNum3;
        for (var k = 0; currentNum <= finishNum3; k++){
            array3.push(currentNum)
            currentNum += p
        }
    }else if (!startNum3 && !!finishNum3 && !!step3){
        return 0
    }else if (startNum3){
        return startNum3
    }
    var total = 0
    for (var p = 0 ; p< array3.length; p++){
        total = total + array3[p]
    }
    return total
}

console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

console.log("Soal 4");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(data){
    var dataLength = data.length
    for (var f=0; f<dataLength; f++){
        var id = "Nomor ID\t:" + data[f][0]
        var nama = "Nama Lengkap\t:" + data[f][1]
        var ttl = "TTL\t\t:" + data [f][2] + " " + data [f][3]
        var hobi = "Hobi\t\t:" + data [f][4]

        console.log(id + "\n" + nama + "\n" + ttl + "\n" + hobi)
    }
  
}
dataHandling(input)

console.log("Soal 5");

function balikKata(kata){
    var kataBaru = "";
    for (var i = kata.length-1; i >=0; i--){
        kataBaru+= kata[i]
    }
    return kataBaru;
}


console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("Soal 6");

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(data){
    var newData = data
    var newName = data[1] + "Elsharawy"
    var newProvince= "Provinsi" + data[2]
    var gender ="Pria"
    var institusi = "SMA International Metro"

    newData.splice(1,1, newName)
    newData.splice(2,1, newProvince)
    newData.splice(4, 1, gender, institusi)

    var arrayBulan = data [3]
    var bulan = arrayBulan.split("/")
    var bulanNum = bulan [1]
    var bulanNama=""

    switch (bulanNum){
        case "01"  :
            monthname = "Januari"
            break;
        case "02"  :
             monthname = "Februari"
            break;
        case "03"  :
             monthname = "Maret"
            break;
        case "04"  :
             monthname = "April"
             break;
         case "05"  :
             monthname = "Mei"
             break;
         case "06"  :
             monthname = "Juni"
             break;
         case "07"  :
             monthname = "Juli"
              break;
         case "08"  :
             monthname = "Agustus"
              break;
         case "09"  :
              monthname = "September"
              break;
          case "10"  :
              monthname = "Oktober"
              break;
         case "11"  :
              monthname = "November"
              break;
          case "12"  :
              monthname = "Desember"
              break;
         default:
             break;
    }   
        var bulanJoin =  bulan.join("-")
        var bulanArr  =  bulan.sort(function(value1, value2){
            value2 - value1
        })
        var editName = newName.slice(0, 15)
        console.log(newData)
        console.log(bulanNama)
        console.log(bulanArr)
        console.log(bulanJoin)
        console.log(editName)

    }
    console.log(input2)

dataHandling2(input2);


