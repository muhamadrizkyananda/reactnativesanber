console.log("-------Soal 1 ------")
//sebelumnya
/*const golden = function goldenFunction(){
  console.log("this is golden!!")
}
 
golden()*/
  //arrow function
 
 
 const golden = goldenFunction = () => {
   console.log("this is golden!!")
 }
 
 golden()

console.log("-------Soal 2-------")

const newFunction = function literal(firstName, lastName){
  return {
   firstName,
   lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 
    
console.log("------Soal 3--------")
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

console.log("--------Soal 4--------")
const { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName, lastName, destination, occupation)

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)

console.log("-------Soal 5-------")
const planet = "earth"
const view = "glass"
const before = `Lorem  ${view} dolor sit amet,
    consectetur adipiscing elit, ${planet} do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam `

 
// Driver Code
console.log(before) 