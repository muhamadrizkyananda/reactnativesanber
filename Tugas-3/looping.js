console.log("LOOPING PERTAMA");
console.log("---------------");
var genap1 = 0
var penambah = 1
while (genap1 < 21){
  console.log(genap1 + "- I love coding");
  genap1 += penambah
  genap1++
    
}
console.log("==============");
console.log("LOOPING KEDUA");
console.log("-------------");

var genap2 = 20
var pengurang = 1
while(genap2 > 0){
  console.log(genap2 + " - I will become mobile developer");
  genap2 -= pengurang
  genap2--
}
  
 
console.log("≠=≠=≠=≠=≠=≠=≠=≠=≠=≠=≠=≠=≠=≠=≠=≠");
console.log("Looping Menggunakan for");
console.log("-------------------------------");
// SYARAT
// A. Jika angka ganjil maka tampilkan Santai
// B. Jika angka genap maka tampilkan Berkualitas
// C. Jika angka yang sedang ditamilkan adalah kelipatan 3 DAN angka ganji maka tampilkan I Love Coding

//Angka dari 1 - 10
var i = 0
for (i=1;i<=20;i++){
  if (i%2==0){
    console.log(i + " - Berkualitas"); //bilangan genap
  }else if(i%3==0){
    console.log(i + " - I Love Coding"); //kelipatan 3 dan ganjil
  }else {
    console.log(i + " - Santai"); // bilangan ganjil
  }
}

console.log("_________________________");
console.log("Membuat Persegi Panjang");
console.log("_________________________");

var cetak1=""
for (var u=1;u<=4;u++){
  for (var m=1;m<=8;m++){
    cetak1 += "#";
  }
  cetak1 += '\n';
}
console.log(cetak1);

console.log("_________________________");
console.log("Membuat Tangga");
console.log("_________________________");


var cetak
for (var x=1;x<=7;x++){
  cetak = "";
  for (var y=1;y<=7;y++){
    if(y <= x) {
      cetak += "#"
    } else {
      cetak += " ";
    }
    
  }
 console.log(cetak);
}


console.log("_________________________");
console.log("Membuat Papan Catur");
console.log("_________________________");

var c = 0
for (c=1;c<=8;c++){
  if(c%2==0){
    console.log("# # # #");
  }else {
    console.log(" # # # #");
  }
}


